FROM docker.io/library/rust:latest AS builder

# Compile alexandrie
RUN git clone https://github.com/d-e-s-o/cargo-http-registry.git && \
    cd cargo-http-registry && \
    git checkout v0.1.5 && \
    cargo build --release

FROM registry.fedoraproject.org/fedora:39

# Install dependencies
RUN dnf install -y --setopt=install_weak_deps=False git-core systemd jq && \
    rm -rf /var/cache/dnf

ADD root/ /root/
ADD etc/ /etc/
COPY --from=builder /cargo-http-registry/target/release/cargo-http-registry /usr/local/bin/

# Make cargo fetch index repo with the git CLI which respects our ssh config
ENV CARGO_NET_GIT_FETCH_WITH_CLI="true" \
    CARGO_HOME="/root/rust/cargo" \
    CARGO_REGISTRIES_SIMULATE_INDEX="file:///root/crate-registry" \
    CARGO_REGISTRIES_SIMULATE_TOKEN="thisisirrelevant" \
    CARGO_REGISTRY_DEFAULT="simulate" \
    RUSTUP_HOME="/root/rust/rustup" \
    EDITOR="vi"

VOLUME "/root/rust"

# NOTE: It's important to attach a TTY to systemd, otherwise things like
# logging **will not work**.
LABEL start="podman run --rm -td \
    --name cargo-release-simulator --replace \
    -v \"\${PWD}/entrypoint.sh:/entrypoint.sh:ro,z\" \
    -v \"simulate-release-data:/root/rust\" \$IMAGE"
LABEL exec="podman exec -it cargo-release-simulator /entrypoint.sh"
LABEL stop="podman stop -i cargo-release-simulator"

# Remove a bunch of broken/default dependencies
RUN rm -rf \
    /etc/dnf/protected.d/* \
    /etc/sytemd/system/getty.target.wants/* \
    /etc/sytemd/system/multi-user.target.wants/* \
    /etc/sytemd/system/sysinit.target.wants/* \
    /etc/sytemd/system/timers.target.wants/* \
    /lib/systemd/system/graphical.target.wants/* \
    /lib/systemd/system/multi-user.target.wants/{getty.target,systemd-ask-password-wall.path} \
    /lib/systemd/system/sys-kernel*.mount

ENTRYPOINT [ "/lib/systemd/systemd" ]
