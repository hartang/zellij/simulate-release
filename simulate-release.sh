#!/usr/bin/env bash
set -ETeuo pipefail

function _info { echo -e "\e[34mINFO\e[0m  $@"; }
function _warn { echo -e "\e[33mWARN\e[0m  $@"; }
function _ok   { echo -e "\e[32m OK \e[0m  $@"; }
function _err  { echo -e "\e[31mERRO\e[0m  $@"; }
function _fatal { _err "$@"; exit 1; }
function _clear { echo -ne "\e[1F\e[2K"; }
function _exists { command -v "$1" &>/dev/null; }
# Error handling/tracing
declare -a ERROR_CTX
function _err_ctx {
    [[ "$#" -ge 1 ]] || _fatal "'_err_ctx' needs at least one arg"
    CMD="$1"
    shift 1
    case "$CMD" in
        "push")
            # Record caller information
            declare -a CALLER
            readarray -td ' ' CALLER <<< "$(caller 0)"
            ERROR_CTX+=("$(echo "$@ (${CALLER[2]%$'\n'}@${CALLER[0]})")")
            ;;
        "pop")
            NERR="${#ERROR_CTX[@]}"
            unset "ERROR_CTX[$((NERR-1))]"
            ;;
        "print")
            for ((i=$((${#ERROR_CTX[@]}-1));i>=0;i--)); do
                _err "${ERROR_CTX[$i]}"
            done
            ;;
        *)
            _fatal "unknown command '$CMD'"
            ;;
    esac
}
trap "_err_ctx print" ERR EXIT
## Cursor manipulation
# Store cursor for subsequent use of `_rc`
function _sc { tput sc; }
# Restore cursor position stored with `_sc` and delete until end of screen
function _rc { tput rc; echo -ne "\e[0J"; }


## Reset to original state on exit/abort
#function _cleanup {
#    _ok "bye"
#}
#trap _cleanup INT QUIT KILL ABRT EXIT TERM

declare -A ARGS=(
    [subcommand]=""
    [registry]=""
    [repo_token]=""
    [repo_user]=""
    [zellij_dir]=""
    [zellij_fork]=""
    [ktra_dir]=""
)

function _usage {
    echo "Usage: $0 [OPTIONS]... SUBCOMMAND"
    echo "Helper script to simulate a zellij release."
    echo ""
    echo "Available options:"
    echo "  -h, --help                  show this help text"
    echo "  -r, --registry URL          git repo for custom cargo registry index"
    echo "  -u, --registry-user NAME    username to use for access to registry repo"
    echo "  -t, --registry-token TOKEN  token for read/write access to registry repo"
    echo "  -z, --zellij-dir DIR        path to zellij repo on disk"
    echo "  -f, --zellij-fork REMOTE    name of remote zellij fork with read/write access"
    echo "  -k, --ktra-dir DIR          path to the ktra config directory (obtained by 'setup')"
    echo ""
    echo "Available subcommands:"
    echo "  setup                       setup an environment for simulated releases"
    echo "  release                     perform a simulated release"
    echo "  reset                       undo all changes for simulating releases"
    echo ""
    echo "The subcommands are meant to be executed in order. If any of the subcommands exits"
    echo "with a non-zero status, DO NOT continue with the next step."
    echo ""
    echo "This script is meant to shorten the process of simulating a zellij release. It was"
    echo "hacked up in comparatively little time (given the complexity of the task) and is"
    echo "not actively developed, so please use it with caution. I recommend that you read"
    echo "and understand the script contents before trusting it blindly. This script is not"
    echo "idempotent, so running it repeatedly may cause weird results (although I did my best"
    echo "to prevent this)."
    echo ""
}

#trap "_fatal 'failed to parse CLI arguments\n${ERROR[@]}'" ERR

_err_ctx push "failed to parse CLI arguments"
[[ -n "$@" ]] || { _usage; _fatal "you must provide at least a subcommand"; }
while [[ "$#" -gt 0 ]]; do
    case "$1" in
        "-h"|"--help")
            _usage
            exit 0
            ;;
        "-r"|"--registry")
            [[ "$#" -ge 2 ]] || _fatal "'$1' needs argument 'URL'"
            [[ "$2" = "https://"* ]] || _fatal "registry URL should be of type 'https://'"
            ARGS[registry]="$2"
            shift 2
            ;;
        "-u"|"--registry-user")
            [[ "$#" -ge 2 ]] || _fatal "'$1' needs argument 'NAME'"
            ARGS[repo_user]="$2"
            shift 2
            ;;
        "-t"|"--registry-token")
            [[ "$#" -ge 2 ]] || _fatal "'$1' needs argument 'TOKEN'"
            ARGS[repo_token]="$2"
            shift 2
            ;;
        "-z"|"--zellij-dir")
            [[ "$#" -ge 2 ]] || _fatal "'$1' needs argument 'DIR'"
            [[ -d "$2" ]] || _fatal "'$2' is not a valid path"
            [[ -d "$2/zellij-client" ]] || \
                _warn "'$2' doesn't look like a zellij repo, continuing..."
            ARGS[zellij_dir]="$2"
            shift 2
            ;;
        "-f"|"--zellij-fork")
            [[ "$#" -ge 2 ]] || _fatal "'$1' needs argument 'REMOTE'"
            ARGS[zellij_fork]="$2"
            shift 2
            ;;
        "-k"|"--ktra-dir")
            [[ "$#" -ge 2 ]] || _fatal "'$1' needs argument 'DIR'"
            [[ -d "$2" ]] || _fatal "'$2' is not a valid path"
            [[ -f "$2/ktra.toml" ]] || \
                _warn "'$2' doesn't look like a ktra directory, continuing..."
            ARGS[ktra_dir]="$2"
            shift 2
            ;;
        -*)
            _usage
            _fatal "unknown argument '$1'"
            ;;
        *)
            [[ -z "${ARGS[subcommand]}" ]] || \
                _fatal "only one subcommand allowed, previous subcommand was '${ARGS[subcommand]}'"
            [[ "$1" = "setup" ]] || [[ "$1" = "release" ]] || [[ "$1" = "reset" ]] || \
                _fatal "unknown subcommand '$1'"
            ARGS[subcommand]="$1"
            shift 1
            ;;
    esac
done
_err_ctx pop

function _do_setup {
    _err_ctx push "failed to setup index repository for custom cargo registry"
    # Query cargo-registry repo from user
    if [[ -n "${ARGS[registry]}" ]]; then
        _info "using provided registry repo '${ARGS[registry]}'"
    else
        URL=""
        _info "In order to simulate a release, we must setup a custom cargo registry."
        _info "This requires a so-called 'index reposiory', which is a git repo that"
        _info "holds a JSON-description of where to find the registry (download URLs)"
        _info "along with metadata for each crate published there. If you haven't done"
        _info "so yet, please provide a git repo now. It needn't be public, but the"
        _info "registry tool (ktra, see later) must access it (e.g. via tokens). Also"
        _info "please make sure the main branch is named 'main' and use 'https://' as"
        _info "clone method."
        echo ""
        while true; do
            read -p "URL to the git repo: " URL || \
                _fatal "error while reading registry URL"
            [[ "$URL" = "https://"* ]] || \
                { _fatal "the registry URL must use the 'https' transport method"; continue; }
            ARGS[registry]="$URL"
            _ok "registry URL '$URL' accepted"
            break
        done
    fi
    # strip https prefix, we glue it back on later
    ARGS[registry]="${ARGS[registry]#"https://"}"

    # Query repo username from user
    if [[ -n "${ARGS[repo_user]}" ]]; then
        _info "using provided registry repo username '${ARGS[repo_user]}'"
    else
        USER=""
        echo ""
        while true; do
            read -p "Username to access index repo: " USER || \
                _fatal "error while reading index repo username"
            [[ -n "$USER" ]] || { _err "username mustn't be empty"; continue; }
            ARGS[repo_user]="$USER"
            _ok "index repo username '$USER' accepted"
            break
        done
    fi
    
    # Query repo token from user
    if [[ -n "${ARGS[repo_token]}" ]]; then
        _info "using provided registry repo token '${ARGS[repo_token]}'"
    else
        TOKEN=""
        echo ""
        _info "For reasons of convenience, the registry tool should be able to access"
        _info "the index repo without interactive authentication. Please create an"
        _info "access token for passwordless read/write access to your index repo"
        echo ""
        while true; do
            read -p "Token to access index repo (r/w): " TOKEN || \
                _fatal "error while reading index repo token"
            [[ -n "$TOKEN" ]] || { _err "token mustn't be empty"; continue; }
            ARGS[repo_token]="$TOKEN"
            _ok "index repo token '$TOKEN' accepted"
            break
        done
    fi

    # Clone the index repo locally
    REPODIR="$(mktemp -d)"
    _sc; _info "cloning registry index into '$REPODIR'"
    pushd "$REPODIR" &>/dev/null
    URL="https://${ARGS[repo_user]}:${ARGS[repo_token]}@${ARGS[registry]}"
    git clone "$URL" index-repo || _fatal "failed to clone index repo from URL '$URL'"
    pushd index-repo &>/dev/null
    _rc; _ok "index repo cloned successfully"

    # Push a test-commit to see if it works
    _sc; _info "adding registry configuration"
    echo '{"dl":"http://localhost:8000/dl","api":"http://localhost:8000"}' > config.json
    git add "config.json" || _fatal "failed to add config file"
    if [[ -n "$(git diff -p)" ]]; then
        git commit -m "deploy registry configuration" || _fatal "failed to create git commit"
        git push || _fatal "failed to push commit to remote"
    else
        _info "config file exists, testing token access"
        TMPFILE="$(mktemp -p "$PWD")"
        echo "access token test" > "$TMPFILE"
        git add "$TMPFILE" || _fatal "failed to add '$TMPFILE' to git commit"
        git commit -m "testing token access rights" || _fatal "failed to commit changes"
        git push || _fatal "failed to push commit to remote"
    fi
    _rc; _ok "index repository configured, access token ok"

    if [[ -d "ze/ll" ]]; then
        _sc; _info "deleting previous zellij releases"
        git rm -rf "ze/ll" || _fatal "failed to remove previous zellij releases"
        git rm -rf "tmp*" || _fatal "failed to remove previous tempfiles"
        git commit -m "remove previous zellij releases" || _fatal "failed to create git commit"
        git push || _fatal "failed to push commit to remote"
        _rc; _ok "previous zellij releases deleted"
    fi

    popd &>/dev/null; popd &>/dev/null
    _err_ctx pop
    _ok "index repository set up and working"

    # Query zellij repo location on disk
    if [[ -n "${ARGS[zellij_dir]}" ]]; then
        _info "using provided zellij repo directory"
    else
        DIR=""
        echo ""
        _info "We need a local checkout of the zellij code repository to perform the"
        _info "publishing with. Please enter the path to a locally checked-out repo"
        _info "below"
        echo ""
        while true; do
            read -e -p "Path to local zellij repo: " DIR || \
                _fatal "error while reading zellij repo directory"
            DIR="$(eval echo $DIR)"
            [[ -e $DIR ]] || { _err "path '$DIR' doesn't exist"; continue; }
            [[ -d $DIR ]] || { _err "'$DIR' is not a directory"; continue; }
            git -C "$DIR" status &>/dev/null || \
                { _err "'$DIR' doesn't appear to be a git repo"; continue; }
            ARGS[zellij_dir]="$DIR"
            _ok "zellij repo-dir '$DIR' accepted"
            break
        done
    fi
    pushd "${ARGS[zellij_dir]}" &>/dev/null

    # Add `ktra` registry in `.cargo/config.toml`
    if grep -q "ktra =" .cargo/config.toml &>/dev/null; then
        _ok "registry 'ktra' already configured in zellij config"
    else
        cat >> .cargo/config.toml <<EOF
[registries]
ktra = { index = "https://${ARGS[registry]}" }
EOF
        _ok "registry 'ktra' configured in zellij config"
    fi

    # Modify all `Cargo.toml`
    for cargo_toml in $(find -type f -iregex '\.\(/\|/zellij.*\)Cargo.toml'); do
        if grep "registry = \"ktra\"" "$cargo_toml" &>/dev/null; then
            _ok "'$cargo_toml' already uses 'ktra' registry"
            continue
        fi
        sed -i 's/zellij-\(.*\) }/zellij-\1, registry = "ktra" }/' "$cargo_toml" || \
            _fatal "failed to rewrite '$cargo_toml' to use 'ktra' registry"
    done

    # Query zellij fork from user
    if [[ -n "${ARGS[zellij_fork]}" ]]; then
        _info "using provided zellij fork"
    else
        REMOTE=""
        echo ""
        _info "Finally we need the name of a configured remote that contains a"
        _info "writable fork of the zellij project. We will perform a release commit"
        _info "and push it there. The fork need't be public, but it must be present"
        _info "and configured in a way that it is accessible (e.g. via SSH)"
        echo ""
        while true; do
            read -p "Name of a configured zellij fork remote: " REMOTE || \
                _fatal "error while reading zellij fork remote"
            git remote get-url "$REMOTE" &>/dev/null || \
                { _err "'$REMOTE' doesn't appear to be a configured git remote"; continue; }
            ARGS[zellij_fork]="$REMOTE"
            _ok "zellij fork '$REMOTE' accepted"
            break
        done
    fi
    popd &>/dev/null

    # Create `~/.cargo/config.toml` with ktra registry
    CARGO_CONFIG="$HOME/.cargo/config.toml"
    MODIFY_CONFIG=0
    if [[ -f "$CARGO_CONFIG" ]]; then
        # Already exists
        if grep -q "CREATED BY SIMULATE-RELEASE.SH" "$CARGO_CONFIG" &>/dev/null; then
            _ok "'$CARGO_CONFIG' already set up"
            MODIFY_CONFIG=1
        else
            _warn "'$CARGO_CONFIG' already exists, appending our changes to it"
        fi
    else
        _info "'$CARGO_CONFIG' doesn't exist yet, creating it now"
    fi
    [[ "$MODIFY_CONFIG" -eq 0 ]] && cat >> "$CARGO_CONFIG" <<EOF
# --- CREATED BY SIMULATE-RELEASE.SH - START ---
[registries.ktra]
index = "https://${ARGS[registry]}"
# --- CREATED BY SIMULATE-RELEASE.SH - END ---
EOF
    _ok "'$CARGO_CONFIG' ok"

    # Comment out existing `registry` token if available
    CARGO_CRED="$HOME/.cargo/credentials.toml"
    if [[ -f "$CARGO_CRED" ]]; then
        _info "hiding '$CARGO_CRED' to prevent accidental release to crates.io"
        mv --backup=numbered "$CARGO_CRED" "$CARGO_CRED.BAK"
        _ok "'$CARGO_CRED' moved to '$CARGO_CRED.BAK'"
    fi

    # Install ktra
    _sc; _info "installing 'ktra' now"
    cargo install --locked --version 0.7.0 ktra || _fatal "failed to install 'ktra'"
    _rc; _ok "'ktra' installed"

    # Create ktra tempdir
    KTRA_DIR="$(mktemp -d)"
    pushd "$KTRA_DIR" &>/dev/null

    # Create ktra.toml
    cat > ktra.toml <<EOF
remote_url = "https://${ARGS[registry]}"
https_username = "${ARGS[repo_user]}"
https_password = "${ARGS[repo_token]}" 
branch = "main"  # Or whatever branch name you used
EOF

    # Copy index repo to ktra folder
    cp -r "$REPODIR/index-repo" "$KTRA_DIR/index" || \
        _fatal "failed to copy index-repo to ktra directory"


    # Launch ktra
    timeout 10 env RUST_LOG=debug ktra &
    KTRA_PID="$!"
    _ok "'ktra' launched successfully"
    sleep 3

    # Create registry token in ktra
    RESPONSE="$(curl -X POST -H 'Content-Type: application/json' \
        -d "{\"password\":\"${ARGS[repo_token]}\"}" \
        "http://localhost:8000/ktra/api/v1/new_user/${ARGS[repo_user]}" || \
        _fatal "failed to request API token from ktra")"
    TOKEN="$(cut -d'"' -f4 <<< "$RESPONSE" || \
        _fatal "failed to retrieve API token from ktra response '$RESPONSE'")"
    _ok "received API token from ktra"

    # Register cargo with registry
    cargo login --registry=ktra "$TOKEN" || _fatal "failed to store API token for ktra registry"
    _ok "successfully registered with ktra registry"
    popd &>/dev/null

    _ok "ktra setup complete in directory '$KTRA_DIR'"

    echo ""
    _ok "Setup for simulated releases is now complete. It is highly recommended to check"
    _ok "a few things before proceeding:"
    _ok "  - Does '~/.cargo/config.toml' contain the ktra registry config?"
    _ok "  - Does '~/.cargo/credentials' NOT contain credentials for crates.io?"
    _ok "  - Do the pending changes in '${ARGS[zellij_dir]}' look right to you?"
    _ok "  - Is there a git remote configured in zellij that allows non-interactive"
    _ok "    pushing to the main repository? If so, you may want to temporarily disable"
    _ok "    it."
    _ok ""
    _ok "If everything seems right to you, you can now perform the simulated release with the"
    _ok "'release' subcommand to this script, like so:"
    _ok ""
    _ok "    $0 release -z '${ARGS[zellij_dir]}' -f '${ARGS[zellij_fork]}' -k '$KTRA_DIR'"
    echo ""
}

# `cargo xtask release --git-remote simulate-fork --cargo-registry ktra
function _do_release {
    # Check all arguments exist
    [[ -n "${ARGS[zellij_dir]}" ]] || \
        _fatal "please provide a zellij directory with the '-z' option"
    [[ -n "${ARGS[zellij_fork]}" ]] || \
        _fatal "please provide a zellij fork with the '-f' option"
    [[ -n "${ARGS[ktra_dir]}" ]] || \
        _fatal "please provide a ktra directory with the '-k' option"

    # Launch ktra
    if [[ -n "$ZELLIJ" ]]; then
        zellij run --cwd "${ARGS[ktra_dir]}" -f -n "ktra server" -- env RUST_LOG=debug ktra
    else
        pushd "${ARGS[ktra_dir]}" &>/dev/null || _fatal "failed to cd to '${ARGS[ktra_dir]}'"
        RUST_LOG=debug ktra &
        KTRA_PID="$!"
        _info "launched ktra with PID '$KTRA_PID'"
        popd &>/dev/null || _fatal "failed to cd back into base directory"
    fi
    sleep 3

    # Run the actual publish
    pushd "${ARGS[zellij_dir]}" &>/dev/null || \
        _fatal "failed to cd to '${ARGS[zellij_dir]}'"
    _sc; _info "performing publish, this may take a while..."
    cargo xtask publish --git-remote "${ARGS[zellij_fork]}" --cargo-registry "ktra" || \
        _fatal "failed to publish zellij project"
    _rc; _ok "publish successful"

    # Check if binary is ok
    _sc; _info "retrieveing published crate"
    cargo install --registry ktra --root /tmp/ zellij || \
        _fatal "failed to install 'zellij' from 'ktra' registry"
    _rc; _ok "'zellij' pre-release installed to '/tmp/bin/zellij'"

    echo ""
    _ok "The publish was successful. You can now test the freshly-released zellij binary"
    _ok "by running '/tmp/bin/zellij'. In addition, the zellij repo fork (assuming it is"
    _ok "hosted in github.com) should now build the release artifacts for various arches."
    _ok "Check back in an hour or so and try whether the GitHub release-binaries work,"
    _ok "too, just to make sure it's all good."
    _ok ""
    _ok "If you can't find anything wrong with the result: Congratulations, it looks like"
    _ok "the simulated release was successful! You can now run a real release with:"
    _ok ""
    _ok "    cargo xtask publish"
    _ok ""
    _ok "Once that's done, continue with cleaning up the mess we made:"
    _ok ""
    _ok "    $0 reset -z '${ARGS[zellij_dir]}' -f '${ARGS[zellij_fork]}' -k '${ARGS[ktra_dir]}'"
    echo ""
}

function _do_reset {
    # Check all arguments exist
    [[ -n "${ARGS[zellij_dir]}" ]] || \
        _fatal "please provide a zellij directory with the '-z' option"
    [[ -n "${ARGS[zellij_fork]}" ]] || \
        _fatal "please provide a zellij fork with the '-f' option"
    [[ -n "${ARGS[ktra_dir]}" ]] || \
        _fatal "please provide a ktra directory with the '-k' option"

    pushd "${ARGS[zellij_dir]}" &>/dev/null || _fatal "failed to cd to '${ARGS[zellij_dir]}'"
    VERSION="v$(grep '^version' Cargo.toml | cut -d'"' -f2)"
    git tag -d "$VERSION" || _fatal "failed to delete git tag '$VERSION'"
    git push --force --delete "${ARGS[zellij_fork]}" "$VERSION" || \
        _fatal "failed to delete git tag '$VERSION' from remote '${ARGS[zellij_fork]}'"

    # Undo the git commit
    git reset --hard HEAD~1 || _fatal "failed to reset git release-commit"
    git tag -d
}

case "${ARGS[subcommand]}" in
    "setup")
        _err_ctx push "failed to setup simulated release"
        _do_setup
        ;;
    "release")
        _err_ctx push "failed to perform simulated release"
        _do_release
        ;;
    "reset")
        _err_ctx push "failed to undo previous changes"
        _do_reset
        ;;
    *)
        _fatal "unknown subcommand '${ARGS[subcommand]}'"
        ;;
esac

trap - ERR EXIT

