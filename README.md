# What is this?

This container aims to provide an environment for "simulating" cargo releases.
It provides a fully-local, ephemeral cargo registry that can be pushed to and
pulled from.

This project originally came to be as part of the zellij application, which,
due to its complexity, has experienced unexpected failures during a few past
releases. Through this effort, we hope to catch issues in the release process
ahead of time. At the same time we want to help other projects in similar
situations get a better hold of such issues.


# How to run

1. Build the container: `podman build -t localhost/simulate .`
2. Start the container: `podman container runlabel start localhost/simulate`
3. Run the entry point: `podman container runlabel exec localhost/simulate`
4. Obtain the built binaries: `podman cp cargo-release-simulator:/tmp/zellij /tmp/zellij`
5. Stop the container: `podman container runlabel stop localhost/simulate`
