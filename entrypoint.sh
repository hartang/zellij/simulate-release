#!/usr/bin/env bash
set -euo pipefail

[[ -n "$RUSTUP_HOME" ]] || exit 64
[[ -n "$CARGO_HOME" ]] || exit 65

# config
reg="$CARGO_REGISTRY_DEFAULT"

# Install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
chmod +x rustup.sh
./rustup.sh -y
source "$CARGO_HOME/env"

# Clone zellij and modify all `Cargo.toml` to use registry
rm -rf zellij &&
    git clone https://github.com/zellij-org/zellij &&
    cd zellij
while read -r cargo_toml; do
    if grep "registry = \"$reg\"" "$cargo_toml" &>/dev/null; then
        _ok "'$cargo_toml' already uses '$reg' registry"
        continue
    fi
    sed -i "s/zellij-\\(.*\\) }/zellij-\\1, registry = \"$reg\" }/" "$cargo_toml" || \
        _fatal "failed to rewrite '$cargo_toml' to use '$reg' registry"
done <<< "$(find . -type f -iregex '\.\(/\|/zellij.*\)Cargo.toml')"

# Install dependencies
dnf install -y gcc musl-gcc openssl-devel perl protobuf-compiler

# Simulate release, go!
cargo xtask publish --cargo-registry "$reg" --no-push

# Install the packages
cargo install --locked --registry "$reg" --root /tmp/zellij --target x86_64-unknown-linux-gnu zellij
mv /tmp/zellij/bin/zellij /tmp/zellij/zellij-x86_64

rustup target add x86_64-unknown-linux-musl
cargo install --locked --registry "$reg" --root /tmp/zellij --target x86_64-unknown-linux-musl zellij
mv /tmp/zellij/bin/zellij /tmp/zellij/zellij-musl
